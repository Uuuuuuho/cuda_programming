#include <iostream>
#include <stdlib.h>

#include <helper_functions.h>
#include "device_launch_parameters.h"

#define N 1048576
#define BLOCK_SIZE 32

__global__ void matrix_transpose_naive(int* input, int* output)
{
  int idx_x = threadIdx.x + blockIdx.x * blockDim.x;
  int idx_y = threadIdx.y + blockIdx.y * blockDim.y;
  int idx = idx_y * N + idx_x;
  int trans_idx = idx_x * N + idx_y;

  // Discoalesced global memory store
  output[trans_idx] = input[idx];
}

__global__ void matrix_transpose_shared(int* input, int* output)
{
  __shared__ int sharedMemory [BLOCK_SIZE][BLOCK_SIZE];

  // global index
  int idx_x = threadIdx.x + blockIdx.x * blockDim.x;
  int idx_y = threadIdx.y + blockIdx.y * blockDim.y;

  // transposed global memory index
  int tidx_x = threadIdx.x + blockIdx.x * blockDim.x;
  int tidx_y = threadIdx.y + blockIdx.y * blockDim.y;

  // local index
  int local_idx_x = threadIdx.x;
  int local_idx_y = threadIdx.y;

  int idx = idx_y * N + idx_x;
  int trans_idx = tidx_y * N + tidx_x;

  // reading from global memory in coalesced manner and performing transpose in shared memory
  sharedMemory[local_idx_x][local_idx_y] = input[idx];

  __syncthreads();

  // writing into global memory in coalesced fashion via transposed data in shared memory
  output[trans_idx] = sharedMemory[local_idx_x][local_idx_y];
}

//basically just fills the array with index.
void fill_array(int *data) {
	for(int idx=0;idx<(N*N);idx++)
		data[idx] = idx;
}

int main()
{
  int *a, *b;
  int *g_a, *g_b;

  int size = N * N * sizeof(int);
  int test_iter = 1000;

  float op_time = 0;
  float op_time_1_epoch = 0;

  // Create Timer
  StopWatchInterface *timer = 0;

  // Record the start event
  sdkCreateTimer(&timer);
  sdkStartTimer(&timer);

  // allocate space for host copies of a, b, c and setup input values
  a = (int *)malloc(size); fill_array(a);
  b = (int *)malloc(size);

  // allocate space for device copies of a, b, c
  cudaMalloc((void **)&g_a, size);
  cudaMalloc((void **)&g_b, size);

  dim3 blocckSize(BLOCK_SIZE,BLOCK_SIZE,1);
  dim3 gridSize(N/BLOCK_SIZE,N/BLOCK_SIZE,1);

  for(int iter = 0; iter < test_iter; iter++)
    matrix_transpose_naive<<<gridSize,blocckSize>>>(g_a,g_b);

  // Compute and Print the Performance
  op_time = sdkGetAverageTimerValue(&timer);
  op_time_1_epoch = op_time / test_iter;

  std::cout << "Private memory operating time: " << op_time_1_epoch << std::endl;

  // Restart timer
  sdkStartTimer(&timer);

  for(int iter = 0; iter < test_iter; iter++)
    matrix_transpose_shared<<<gridSize,blocckSize>>>(g_a,g_b);

  // Compute and Print the Performance
  op_time = sdkGetAverageTimerValue(&timer);
  op_time_1_epoch = op_time / test_iter;

  std::cout << "Shared memory operating time: " << op_time_1_epoch << std::endl;

  // Waits for GPu operation finish and recored the time
  sdkStopTimer(&timer);

  cudaMemcpy(b, g_b, size, cudaMemcpyDeviceToDevice);

  free(a);
  free(b);
  cudaFree(g_a);
  cudaFree(g_b);
}


