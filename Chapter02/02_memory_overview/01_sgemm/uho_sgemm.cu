#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include <helper_functions.h>

#define BLOCK_DIM_X 1
#define BLOCK_DIM_Y 1

// =========================================================
// Compute reference data set matrix multiplication on GPU
// C = alpha * A * B + beta * C
// @param A          matrix A as provided to device
// @param B          matrix B as provided to device
// @param C          matrix C as provided to device
// @param N          height of matrix A and matrix C
// @param M          width of matrix B and matrix C
// @param K          width of matrix A and height of matrix C
// @param alpha      scala value for matrix multiplication
// @param beta       scala value for matrix summation with C
// =========================================================

__global__ void sgemm_gpu_kernel
(const float *A, const float *B, float *C, int N, int M, int K, float alpha, float beta)
{
  int col = blockIdx.x * blockDim.x + threadIdx.x;
  int row = blockIdx.y * blockDim.y + threadIdx.y;

  float sum = 0.f;
  for(int i = 0; i < K; i++)
    sum += A[row * K + i] * B[i * K + col];

  C[row * M + col] = alpha * sum + beta * C[row * M + col];
}

void sgemm_gpu
(const float* A, const float* B, float *C, int N, int M, int K, float alpha, float beta)
{
  dim3 dimBlock(BLOCK_DIM_X, BLOCK_DIM_Y);
  dim3 dimGrid(M / dimBlock.x, N / dimBlock.y);
  sgemm_gpu_kernel <<<dimGrid, dimBlock>>> (A, B, C, N, M, K, alpha, beta);
}

void random_init(float *data, int size)
{
  for (int i = 0; i < size; i++)
    // Masking for 8 bits
    data[i] = (rand() & 0xFF) / (float)RAND_MAX;
}

void performance_estimation(void(*sgemm)(const float *, const float *, float *, int, int, int, float, float),
	const float *A, const float *B, float *C, int N, int M, int K, float alpha, float beta)
  // Be carefult of the indents!
{
  int test_iter = 1000;

  // Create Timer
  StopWatchInterface *timer = 0;

  // initial start an operation as a warm start
  sgemm(A, B, C, N, M, K, alpha, beta);

  // Record the start event
  sdkCreateTimer(&timer);
  sdkStartTimer(&timer);

  // Operation Body
  for (int i = 0; i < test_iter; i ++)
    sgemm(A, B, C, N, M, K, alpha, beta);

  // Waits for GPu operation finish and recored the time
  sdkStopTimer(&timer);

  // Compute and Print the Performance
  float op_time = sdkGetAverageTimerValue(&timer);
  float op_time_1_epoch = op_time / test_iter;

  std::cout << "Operation Time = " << op_time_1_epoch << std::endl;

  sdkDeleteTimer(&timer);
}

int main()
{
  float *A, *B, *C;
  float *g_A, *g_B, *g_C;
  int N, M, K;
  float alpha = 2.f;
  float beta = 1.f;
  N = M = K = 256;

  A = (float *)malloc(N * K * sizeof(float));
  B = (float *)malloc(K * M * sizeof(float));
  C = (float *)malloc(N * M * sizeof(float));

  cudaMalloc((void **)&g_A, N * K * sizeof(float));
  cudaMalloc((void **)&g_B, K * M * sizeof(float));
  cudaMalloc((void **)&g_C, N * M * sizeof(float));

  random_init(A, N * K);
  random_init(B, K * M);
  random_init(C, N * M);

  cudaMemcpy(g_A, A, N * K * sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(g_B, B, K * M * sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(g_C, C, N * M * sizeof(float), cudaMemcpyHostToDevice);

  // Do Operation
  // sgemm_gpu(g_A, g_B, g_C, N, M, K, alpha, beta);
  performance_estimation(sgemm_gpu, g_A, g_B, g_C, N, M, K, alpha, beta);

  cudaFree(g_A);
  cudaFree(g_B);
  cudaFree(g_C);

  free(A);
  free(B);
  free(C);

}