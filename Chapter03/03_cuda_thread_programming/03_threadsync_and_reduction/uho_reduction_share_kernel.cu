#include <stdio.h>
#include "reduction.h"

/* 
    parallel sum reduction using shared memory
    - task log(n) steps for n input elements
    - use n threads
    - only works for power of 2 arrays
*/

// cuda thread synchronization
__global__
void reduction_kernel(float *d_out, float * d_in, unsigned int size)
{
  unsigned int idx_x = blockIdx.x * blockDim.x + threadIdx.x;

  extern __shared__ float s_data[];

  s_data[threadIdx.x] = (idx_x < size) ? d_in[idx_x] : 0.f;

  __syncthreads();

  for(unsigned int stride = 1; stride < blockDim.x; stride *= 2)
  {
    // thread synchronous reduction
    if((idx_x % (stride * 2)) == 0)
      s_data[threadIdx.x] += s_data[threadIdx.x + stride];

    __syncthreads();
  }

  if(threadIdx.x == 0)
    d_out[blockIdx.x] = s_data[0];
}

void reduction(float *d_out, float *d_in, int num_threads, int size)
{
  cudaMemcpy(d_out, d_in, size * sizeof(float), cudaMemcpyDeviceToDevice);
  while(size > 1)
  {
    int num_blocks = (size + num_threads - 1) / num_threads;
    reduction_kernel<<<num_blocks, num_threads, num_threads * sizeof(float), 0>>>(d_out, d_out, size);
    size = num_blocks;
  }
}