#include <stdio.h>
#include "reduction.h"

// cuda thread synchronization
__global__ void
reduction_kernel_1(float* g_out, float* g_in, unsigned int size)
{
  unsigned int idx_x = blockIdx.x * blockDim.x + threadIdx.x;

  extern __shared__ float s_data[];

  s_data[threadIdx.x] = (idx_x < size) ? g_in[idx_x] : 0.f;

  __syncthreads();

  for(unsigned int stride = 1; stride < blockDim.x; stride *= 2)
  {
    int idx =  2 * stride * threadIdx.x;

    if(idx < blockDim.x)
      s_data[idx] += s_data[idx + stride];

    __syncthreads();
  }

  if(threadIdx.x == 0)
    g_out[blockIdx.x] = s_data[0];
}

int reduction(float *g_outPtr, float *g_inPtr, int size, int num_threads)
{
  int num_blocks = (size + num_threads - 1) / num_threads;
  reduction_kernel_1<<<num_blocks, num_threads, num_threads * sizeof(float), 0>>>(g_outPtr, g_inPtr, size);
  return num_blocks;
}