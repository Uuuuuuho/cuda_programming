#include <stdio.h>
#include "reduction.h"

#define NUM_LOAD 4

__inline__ __device__ float warp_reduce_sum(float val)
{
  for (int offset = warpSize / 2; offset > 0; offset >>= 1)
  {
    unsigned int mask = __activemask();
    val += __shfl_down_sync(mask, val, offset);    
  }
  return val;
}

__inline__ __device__ float block_reduce_sum(float val)
{
  static __shared__ float shared[32];  // Shared mem for 32 partial sums
  int lane = threadIdx.x % warpSize;
  int wid = threadIdx.x / warpSize;

  // Each warp performs partial reduction
  val = warp_reduce_sum(val);

  if(lane == 0) shared[wid] = val;  // Write reduced value to shared memory

  __syncthreads();    // Wait for all partial reductions

  // Read from the shared memory only if that warp existed
  if(wid == 0){
    val = (threadIdx.x < blockDim.x / warpSize) ? shared[lane] : 0;
    val = warp_reduce_sum(val);     // Final reduce within first warp
  }

  return val;
}

// cuda thread synchronization
__global__ void
reduction_kernel(float *g_out, float *g_in, unsigned int size)
{
  unsigned int idx_x = blockIdx.x * blockDim.x + threadIdx.x;

  // Cumulates input with grid-stride loop and save to shared memory
  float sum[NUM_LOAD] = {0.f};
  for(int i = idx_x; i < size; i+= blockDim.x * gridDim.x * NUM_LOAD)
  {
    for(int step = 0; step < NUM_LOAD; step++)
      sum[step] += (i + step * blockDim.x * gridDim.x < size) ? g_in[i + step * blockDim.x * gridDim.x] : 0.f;
  }
  for(int i = 1; i < NUM_LOAD; i++)
    sum[0] += sum[i];
  // warp synchronous reduction
  sum[0] = block_reduce_sum(sum[0]);

  if(threadIdx.x == 0) g_out[blockIdx.x] = sum[0];
}

void reduction(float *g_outPtr, float *g_inPtr, int size, int num_threads)
{
  int num_sms;
  int num_blocks_per_sm;
  cudaDeviceGetAttribute(&num_sms, cudaDevAttrMultiProcessorCount, 0);
  cudaOccupancyMaxActiveBlocksPerMultiprocessor(&num_blocks_per_sm, reduction_kernel, num_threads, num_threads*sizeof(float));
  int num_blocks = min(num_blocks_per_sm * num_sms, (size + num_threads -1) / num_threads);

  reduction_kernel<<<num_blocks, num_threads>>>(g_outPtr, g_inPtr, size);
  reduction_kernel<<<1, num_threads, num_threads * sizeof(float), 0>>>(g_outPtr, g_inPtr, num_blocks);
}