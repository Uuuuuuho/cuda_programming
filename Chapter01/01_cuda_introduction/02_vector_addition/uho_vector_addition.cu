#include <iostream>
#include <stdlib.h>

#define N 512

void host_add(int *a, int *b, int *c)
{
  for(int idx = 0; idx < N; idx++)
  {
    c[idx] = a[idx] + b[idx];
  }
}

void fill_arr(int *data)
{
  for(int idx = 0; idx < N; idx++)
  {
    data[idx] = idx;
  }
}

void print_output(int *a, int *b, int *c)
{
  for(int idx = 0; idx < N; idx++)
  {
    std::cout << a[idx] << " " << b[idx] << " " << c[idx] << std::endl;
  }
}

int main()
{
  int *a, *b, *c;
  int size = N * sizeof(unsigned int);

  a = (int*)malloc(size); fill_arr(a);
  b = (int*)malloc(size); fill_arr(b);
  c = (int*)malloc(size);

  host_add(a,b,c);

  print_output(a,b,c);

  free(a); free(b); free(c);

}