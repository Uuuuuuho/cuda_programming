#include <iostream>
#include <stdlib.h>

#include <helper_functions.h> // for benchmark purpose

#define N 512

void host_add(int *a, int *b, int *c)
{
  for(int idx = 0; idx < N; idx++)
  {
    c[idx] = a[idx] + b[idx];
  }
}

void fill_arr(int *data)
{
  for(int idx = 0; idx < N; idx++)
  {
    data[idx] = idx;
  }
}

void print_output(int *a, int *b, int *c)
{
  for(int idx = 0; idx < N; idx++)
  {
    std::cout << a[idx] << " " << b[idx] << " " << c[idx] << std::endl;
  }
}

__global__ void gpu_add(int *a, int *b, int *c)
{
  c[threadIdx.x] = a[threadIdx.x] + b[threadIdx.x];
}

int main()
{
  // variables
  int *a, *b, *c;
  int *g_a, *g_b, *g_c;
  int size = N * sizeof(unsigned int);
  int thread_per_block = 0, num_blocks = 0;

  // benchmark purpose
	StopWatchInterface *timer = 0;
  

  a = (int*)malloc(size); fill_arr(a);
  b = (int*)malloc(size); fill_arr(b);
  c = (int*)malloc(size);

  // Alloc space for device copies of a,b,c
  cudaMalloc((void **)&g_a, size);
  cudaMalloc((void **)&g_b, size);
  cudaMalloc((void **)&g_c, size);

  cudaMemcpy(g_a, a, size, cudaMemcpyHostToDevice);
  cudaMemcpy(g_b, b, size, cudaMemcpyHostToDevice);

  thread_per_block = 4;
  num_blocks = N/thread_per_block;

	// Record the start event
	sdkCreateTimer(&timer);
  
  // start timer
	sdkStartTimer(&timer);

  gpu_add<<<1,N>>>(g_a,g_b,g_c);

	// Waits for GPU operation finish and recored the time
	sdkStopTimer(&timer);

	// Compute and print the performance
	float operation_time = sdkGetAverageTimerValue(&timer);
	float operation_time_1_epoch = operation_time;

  std::cout << "Thread Only Operation Time = " << operation_time_1_epoch << "msec" << std::endl;

  // start timer
	sdkStartTimer(&timer);

  gpu_add<<<N,1>>>(g_a,g_b,g_c);

	// Waits for GPU operation finish and recored the time
	sdkStopTimer(&timer);

	// Compute and print the performance
	 operation_time = sdkGetAverageTimerValue(&timer);
	operation_time_1_epoch = operation_time;

  std::cout << "Block Only Operation Time = " << operation_time_1_epoch << "msec" << std::endl;

  // start timer
	sdkStartTimer(&timer);

  gpu_add<<<num_blocks,thread_per_block>>>(g_a,g_b,g_c);

	// Waits for GPU operation finish and recored the time
	sdkStopTimer(&timer);

	// Compute and print the performance
	operation_time = sdkGetAverageTimerValue(&timer);
	operation_time_1_epoch = operation_time;

  std::cout << "Thread + Block Operation Time = " << operation_time_1_epoch << "msec" << std::endl;  


	// cleanup
	sdkDeleteTimer(&timer);

  cudaMemcpy(c, g_c, size, cudaMemcpyDeviceToHost);

  // print_output(a,b,c);

  free(a);free(b);free(c);
  cudaFree(g_a);cudaFree(g_b);cudaFree(g_c);
}